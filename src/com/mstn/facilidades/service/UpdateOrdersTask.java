package com.mstn.facilidades.service;

import static com.mstn.facilidades.utils.Utils.SERVER_URL;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mstn.facilidades.OrdenesList;
import com.mstn.facilidades.R;
import com.mstn.facilidades.comm.SendDataTask;
import com.mstn.facilidades.preferences.AppValues;
import com.mstn.facilidades.utils.Utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateOrdersTask extends AsyncTask<Intent, Integer, String> {
    // Do the long-running work in here
	private Context activity;
	
	private static final String TAG = "UpdateOrdersTask";
	
	public UpdateOrdersTask(Context activity){
		this.activity = activity;
	}
	
	/**
     * Funcion que ejecutara en el background recibe strings con:
     * [0] URL del servicio
     * [1] tarjeta del usuario
     * [2] regId de la equipo
     * [3] Data adicional del requerimiento en JSON
    */	
    protected String doInBackground(Intent... intent) {
    	
        //String message = intent.getStringExtra("value");
        String message = "";
        try {
        	Log.i(TAG, "Service Task Started");
			JSONObject mess = new JSONObject();
			mess.put("type","newOrders");
			mess.put("message", "Ordenes Nuevas");
			if (mess.getString("type").equals("newOrders")){
				AppValues appValues = new AppValues(activity);
				String tarjeta = appValues.getString("tarjeta");
				String regId = appValues.getString("regId");
	        	//AsyncTask<String, Integer, String[]> task = new SendDataTask(activity).execute(SERVER_URL + "/ws/OrdersGet", tarjeta, regId); 
	        	//AsyncTask<String, Integer, String> task = new GetOrdersTask().execute(getString(R.string.url_registration) + "/OrdenesGet.html", tarjeta, regId);
	        	//message = task.get()[1];
				message = Utils.sendData(activity, SERVER_URL + "/ws/OrdersGet", tarjeta, regId)[1];
	        	JSONObject orderlist = new JSONObject(message);
	        	JSONArray orders = orderlist.getJSONArray("OrdenesAsignadas");
	        	if (orders.length() == 0){
	        		return "";
	        	}
	        	List<String> savedOrders = new ArrayList<String>();
	        	savedOrders.add(SERVER_URL + "/ws/OrdersUpdate");
	        	
	        	savedOrders.add(tarjeta);
	        	savedOrders.add(regId);
	        	JSONArray processedOrders = new JSONArray();
	        	for(int i = 0;i < orders.length();i++){
	        		JSONObject savedOrder = new JSONObject();
	        		JSONObject order = Utils.addOrders(orders.getJSONArray(i), activity, intent[0]);
	        		if (order != null){
	        			savedOrder.put("Codigo", order.getString("Codigo"));
	        			savedOrder.put("TIPO_ORDEN", order.getString("TIPO_ORDEN"));
	        			processedOrders.put(savedOrder);
	        		}
	        	}
	        	savedOrders.add(processedOrders.toString());
	        	String[] taskData = savedOrders.toArray(new String[savedOrders.size()]);
	        	
	        	//AsyncTask<String, Integer, String[]> taskUpdate = new SendDataTask(activity).execute(taskData);
	        	Utils.sendData(activity, taskData);
	        	//String[] result = taskUpdate.get();
	        	//result.toString();
			}
			message = mess.getString("message");
			Log.i(TAG, "Service Task Ended");
			generateNotification(activity, message);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			//message = e.toString();
		}

    	
    	return message;
    }
    

    private static void generateNotification(Context context, String message) {
        //int icon = R.drawable.ic_stat_gcm;
    	int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, OrdenesList.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }

    }