package com.mstn.facilidades.service;


import com.mstn.facilidades.preferences.AppValues;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class UpdateService extends Service {

	private static final String TAG = "UpdateService";
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		boolean regComplete = (new AppValues(this).getBoolean("registration"));
		Log.i(TAG, "Service Started");
		if (!regComplete){
			Log.i(TAG, "device not registered");
			return START_NOT_STICKY;
		}
		
		AsyncTask<Intent, Integer, String> task = new UpdateOrdersTask(this);
		task.execute(intent);
		Log.i(TAG, "Service Ended");        

		// notifies user
		return Service.START_NOT_STICKY;
		
	};
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
}
