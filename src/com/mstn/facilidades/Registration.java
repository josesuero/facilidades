package com.mstn.facilidades;

import static com.mstn.facilidades.utils.Utils.SERVER_URL;

import org.json.JSONObject;

import com.mstn.facilidades.comm.SendDataTask;
import com.mstn.facilidades.preferences.AppValues;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

/**
 * Actividad de registro recibe el ID de registro de 
 * Google GCM, Solicita la tarjeta del usuario y envia
 * la informacion al servidor. 
 * @author Jose Suero
 *
 */
public class Registration extends Activity {
	/**
	 * Variable global para identificar la actividad en el LOG
	 */
	private ProgressDialog pDialog;
	
	private static String TAG = "Registration";
	
	BroadcastReceiver registrationRequest = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			try{
				Bundle extras = intent.getExtras();
				String responseText = extras.getString("responseText");
				String tarjeta = extras.getString("tarjeta");
	        	Log.i(TAG,responseText.toString());
	        	JSONObject responseJS = new JSONObject(responseText.toString());
	        	if (responseJS.getInt("status") == 1 || responseJS.getInt("status") == 2){
		        	Toast.makeText(context, responseJS.getString("message"), Toast.LENGTH_LONG).show();
	        		(new AppValues(context)).put("registration", true);
		        	(new AppValues(context)).put("tarjeta", tarjeta);
		        	finish();
		        	startActivity(new Intent(context,OrdenesList.class));
	        	} else {
	        		Toast.makeText(context, responseJS.getString("message"), Toast.LENGTH_LONG).show();
	        	}
			} catch(Exception e){
	        	Toast.makeText(context, "Error en el registro", Toast.LENGTH_LONG).show();
	        	Log.e(TAG, e.toString());
			}
			pDialog.dismiss();
		}
	};

	/**
	 * Evento de Creaci�n de pantalla, muestra el formulario
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration_edit);
		registerReceiver(registrationRequest, new IntentFilter("register"));
		
		EditText editText = (EditText)findViewById(R.id.tarjeta);
		editText.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	        if(actionId==EditorInfo.IME_ACTION_DONE){
	            register(findViewById(R.id.tarjeta));
	        }
		    return false;
			}        
		});
	}

	/**
	 * Evento de creacion de menu, agrega las opciones de 
	 * menu.registration_edit, menu) 
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_edit, menu);
		return true;
	}
	
	/**
	 * Funcion para ejecutar el registro en el servidor.
	 * Recibe el regisro y marca las variables de aplicacion registration, tarjeta y regId
	 * registration almacena si la aplicaci�n esta debidamente registrada con el servidor
	 * tarjeta almacena la tarjeta del usuario
	 * regId almacena el ID del telefono en GCM
	 * @param view
	 */
	public void register(View view){
		EditText tarjetaObj = (EditText)findViewById(R.id.tarjeta);
		String tarjeta = tarjetaObj.getText().toString();
		String regId = getIntent().getStringExtra("regId");
		if (regId.equals("")){
			regId = (new AppValues(this)).getString("regId");
		}
		if (regId.equals("")){
			Log.e(TAG, "Empty RegId");
			Toast.makeText(this, "Problemas en el registro", Toast.LENGTH_LONG).show();
		} else if (tarjeta.equals("")){
			Toast.makeText(this, "Debe entrar una tarjeta", Toast.LENGTH_LONG).show();
		} else {
			Log.v(TAG, "Tarjeta = " + tarjeta);
			Log.v(TAG, "regId = " + regId);
			
		    pDialog = ProgressDialog.show(this, "Registrando..", "Por favor espere", true,false);

	        try{
	        	new SendDataTask(this).execute(SERVER_URL + "/ws/register", tarjeta, regId);
	        } catch(Exception e){
	        	pDialog.dismiss();
	        	Toast.makeText(this, "Error en el registro", Toast.LENGTH_LONG).show();
	        	Log.e(TAG, e.toString());
	        }
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(registrationRequest);
	}

}
