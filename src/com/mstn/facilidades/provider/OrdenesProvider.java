package com.mstn.facilidades.provider;

import java.util.HashMap;

import com.mstn.facilidades.db.Ordenes;
import com.mstn.facilidades.db.Ordenes.Orden;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 * Proveedor de contenido para bases de datos
 * @author Jose Suero
 *
 */
public class OrdenesProvider extends ContentProvider {

	/**
	 * Tag para LOG
	 */
	private static final String TAG = "OrdenesProvider";

	/**
	 * Nombre de la base de datos
	 */
	private static final String DATABASE_NAME = "facilidades.db";
	/**
	 * Version de la base de datos
	 */
	private static final int DATABASE_VERSION = 5;
	/**
	 * Nombre de la tabla de ordenes
	 */
	private static final String ORDEN_TABLE_NAME = "ordenes";

	/**
	 * Campos a extraer de tabla de ordenes
	 */
	private static HashMap<String, String> sOrdenProjectionMap;

	/**
	 * URI de proveedor para ordenes
	 */
	private static final int ORDEN = 1;
	/**
	 * URI de proveedor para ordenes por ID
	 */
	private static final int ORDEN_ID = 2;

	/**
	 * Variable para determinar el tipo de URI
	 */
	private static final UriMatcher sUriMatcher;

	/**
	 * Clase para asistencia de manejo de sqlite
	 * @author Jose Suero
	 *
	 */
	private static class DatabaseHelper extends SQLiteOpenHelper {

		/**
		 * Construtor
		 * @param context
		 */
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		/**
		 * Evento al crear la base de datos
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + ORDEN_TABLE_NAME + " (" + Orden._ID
					+ " INTEGER PRIMARY KEY," + Orden.ORDERTYPE + " INTEGER,"
					+ Orden.CODE + " TEXT," + Orden.CLIENT + " TEXT,"
					+ Orden.PRIORITY + " INTEGER,"
					+ Orden.STATUS + " INTEGER,"
					+ Orden.FECHA_COMPROMISO + " TEXT,"
					+ Orden.FACILIDADES + " TEXT,"
					+ Orden.FECHA_FACILIDADES + " TEXT,"
					+ Orden.DATA + " TEXT"
					+ ");");
			db.execSQL("CREATE TABLE IF NOT EXISTS CONFIG (NAME TEXT, VALUE TEXT);");
		}

		/**
		 * Evento al actualizar la base de datos
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + ORDEN_TABLE_NAME);
			onCreate(db);
		}
	}

	/**
	 * Variable para asistir en la base de datos
	 */
	private DatabaseHelper mOpenHelper;
	
    /**
     * Evento al crear
     */
	@Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    /**
     * funcion para hacer una busqueda en la base de datos
     */
	@Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(ORDEN_TABLE_NAME);

        switch (sUriMatcher.match(uri)) {
        case ORDEN:
            qb.setProjectionMap(sOrdenProjectionMap);
            break;

        case ORDEN_ID:
            qb.setProjectionMap(sOrdenProjectionMap);
            qb.appendWhere(Orden._ID + "=" + uri.getPathSegments().get(1));
            break;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // If no sort order is specified use the default
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = Ordenes.Orden.DEFAULT_SORT_ORDER;
        } else {
            orderBy = sortOrder;
        }

        // Get the database and run the query
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);

        // Tell the cursor what uri to watch, so it knows when its source data changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    /**
     * Funcion para determinar el tipo de URI
     */
	@Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
        case ORDEN:
            return Orden.CONTENT_TYPE;

        case ORDEN_ID:
            return Orden.CONTENT_ITEM_TYPE;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    /**
     * Funcion para insertar una orden en la tabla
     */
	@Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        // Validate the requested uri
        if (sUriMatcher.match(uri) != ORDEN) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        //Long now = Long.valueOf(System.currentTimeMillis());

        // Make sure that the fields are all set
        /*
        if (values.containsKey(Ordenes.Orden.DATA) == false) {
            values.put(Ordenes.Orden.CREATED, now);
        }
        */

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert(ORDEN_TABLE_NAME, Orden.CLIENT, values);
        if (rowId > 0) {
            Uri ordenUri = ContentUris.withAppendedId(Ordenes.Orden.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(ordenUri, null);
            return ordenUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }

    /**
     * Funcion para eliminar una orden de la tabla
     */
	@Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
        case ORDEN:
            count = db.delete(ORDEN_TABLE_NAME, where, whereArgs);
            break;

        case ORDEN_ID:
            String ordenId = uri.getPathSegments().get(1);
            count = db.delete(ORDEN_TABLE_NAME, Orden._ID + "=" + ordenId
                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
            break;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    /**
     * Funcion para actualizar una orden en la table
     */
	@Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
        case ORDEN:
            count = db.update(ORDEN_TABLE_NAME, values, where, whereArgs);
            break;

        case ORDEN_ID:
            String ordenId = uri.getPathSegments().get(1);
            count = db.update(ORDEN_TABLE_NAME, values, Orden._ID + "=" + ordenId
                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
            break;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

	/**
	 * Variable staticas para deterinar el tipo de URI
	 */
	static {
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(Ordenes.AUTHORITY, "orden", ORDEN);
		sUriMatcher.addURI(Ordenes.AUTHORITY, "orden/#", ORDEN_ID);

		sOrdenProjectionMap = new HashMap<String, String>();
		sOrdenProjectionMap.put(Orden._ID, Orden._ID);
		sOrdenProjectionMap.put(Orden.CODE, Orden.CODE);
		sOrdenProjectionMap.put(Orden.ORDERTYPE, Orden.ORDERTYPE);
		sOrdenProjectionMap.put(Orden.PRIORITY, Orden.PRIORITY);
		sOrdenProjectionMap.put(Orden.CLIENT, Orden.CLIENT);
		sOrdenProjectionMap.put(Orden.STATUS, Orden.STATUS);
		sOrdenProjectionMap.put(Orden.FECHA_COMPROMISO, Orden.FECHA_COMPROMISO);
		sOrdenProjectionMap.put(Orden.FACILIDADES, Orden.FACILIDADES);
		sOrdenProjectionMap.put(Orden.FECHA_FACILIDADES, Orden.FECHA_FACILIDADES);
		sOrdenProjectionMap.put(Orden.DATA, Orden.DATA);
	}
}
