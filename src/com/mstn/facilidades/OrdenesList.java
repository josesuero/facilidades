package com.mstn.facilidades;

import static com.mstn.facilidades.utils.Utils.DISPLAY_MESSAGE_ACTION;

import static com.mstn.facilidades.utils.Utils.EXTRA_MESSAGE;

import com.mstn.facilidades.adapters.OrderListAdapter;
import com.mstn.facilidades.db.Ordenes.Orden;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import android.widget.Toast;

/**
 * Actividad que muestra la lista de ordenes disponibles
 * Se cargan de la tabla ordenes almacenada localmente en el movil
 * @author Jose Suero
 *
 */
public class OrdenesList extends ListActivity {

    /**
     * Variable Global que al almacena la opcion de eliminar del menu
     * En desuso
     */
	public static final int MENU_ITEM_DELETE = Menu.FIRST;
    /**
     * Variable Global que al almacena la opcion de insertar del menu
     * En desuso
     */    
	public static final int MENU_ITEM_INSERT = Menu.FIRST + 1;
    /**
     * Variable Global que al almacena los campos a mostrar en la tabla principal
     */
	private static final String[] PROJECTION = new String[] {
        Orden._ID, // 0
        Orden.CODE, // 1
        Orden.CLIENT, //2
        Orden.STATUS, //3
        Orden.FECHA_COMPROMISO //4
	};
	//private static final String TAG="Facilidades";
	
	/**
	 * Tarea para registrar la aplicacion
	 */
	AsyncTask<Void, Void, Void> mRegisterTask;
	
    /**
     * Evento de creacion de la actividad
     * Registra los receptores de mensaje en android para provedores
     * Carga las ordenes disponibles
     */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiver(mHandleMessageReceiver,
                new IntentFilter(DISPLAY_MESSAGE_ACTION));
        
        setContentView(R.layout.ordenes_list);
        
        Intent intent = getIntent();
        if (intent.getData() == null) {
            intent.setData(Orden.CONTENT_URI);
        }
        getListView().setOnCreateContextMenuListener(this);
        
        Cursor cursor = getContentResolver().query(getIntent().getData(), PROJECTION, null, null,Orden.STATUS);
        //Cursor cursor = managedQuery(getIntent().getData(), PROJECTION, null, null,Orden.DEFAULT_SORT_ORDER);
        
        OrderListAdapter olAdapter = new OrderListAdapter(this, R.layout.rowlayout, cursor,
                new String[] { Orden.CODE,Orden.CLIENT, Orden.FECHA_COMPROMISO }, new int[] { R.id.OrderCode,R.id.ClientName, R.id.FechaCompromiso });
        setListAdapter(olAdapter);
    }
    
	/**
	 * recibe un mensaje externo para el provedor de ordenes
	 */
    private final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            Toast.makeText(context, newMessage, Toast.LENGTH_LONG).show();
            //mDisplay.append(newMessage + "\n");
        }
    };

    /**
     * Evento de destruccion de la actividad
     */
    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        unregisterReceiver(mHandleMessageReceiver);
        super.onDestroy();
    }
    
    /**
     * Evento de creacion del menu
     * Agrega una opcion para la actividad de preferencias
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ordenes_list, menu);
        return true;
    }

    /**
     * Evento al seleccionar opcion del menu, 
     * primera opcion va a la pantalla de preferencias
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.preferences:
            // Launch activity to insert a new item
            startActivity(new Intent(this,Preferences.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Evento al hacer click sobre la lista, carga el ID del objeto
     * y lo envia a la pantalla de ordenDetail.
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Uri uri = ContentUris.withAppendedId(getIntent().getData(), id);
        
        String action = getIntent().getAction();
        if (Intent.ACTION_PICK.equals(action) || Intent.ACTION_GET_CONTENT.equals(action)) {
            // The caller is waiting for us to return a note selected by
            // the user.  The have clicked on one, so return it now.
            setResult(RESULT_OK, new Intent().setData(uri));
        } else {
            // Launch activity to view/edit the currently selected item
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    }
  
    /*
    public void onClick(View view){
    	switch(view.getId()){
    	case R.id.add:
    	    //int mState;
    		JSONObject order = new JSONObject();
    		Uri mUri = null;

    	    try {
				order.put("cliente", "cliente 2");
	    	    order.put("codigo", "23423");
	    	    order.put("data", "{'ORDEN NO':'123','CLIENTE':'JUAN SANCHEZ','CAMPO1':'DATA CAMPO1',CAMPO2:'HOLA HOLA'}");
	    	    order.put("tipo", "1");

	    		
	    	    mUri = Utils.addOrders(order, this , getIntent());
    	    } catch (Exception e) {
				e.printStackTrace();
			}
            if (mUri == null) {
                Log.e(TAG, "Failed to insert new order into " + getIntent().getData());
                finish();
                return;
            }

            // The new entry was created, so assume all will end well and
            // set the result to be returned.
            //setResult(RESULT_OK, (new Intent()).setAction(mUri.toString()));

    		break;
    	case R.id.delete:
    		if (getListAdapter().getCount() > 0){
    			Cursor cursor = (Cursor)getListAdapter().getItem(getListAdapter().getCount() -1);
        		//Toast.makeText(this, cursor.getString(0), Toast.LENGTH_LONG).show();
    			getContentResolver().delete(Orden.CONTENT_URI, Orden.ID + " = " + cursor.getString(0), null);
    		}
    		break;
    	}
    }
*/
}
