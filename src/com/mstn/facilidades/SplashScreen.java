package com.mstn.facilidades;

import static com.mstn.facilidades.utils.Utils.SERVER_URL;
import static com.mstn.facilidades.utils.Utils.SENDER_ID;

import java.util.Calendar;

import com.mstn.facilidades.comm.SendDataTask;
import com.mstn.facilidades.preferences.AppValues;

import com.mstn.facilidades.service.UpdateService;

import com.mstn.facilidades.update.UpdateAPK;
import com.mstn.facilidades.utils.Utils;


import android.os.Bundle;
import android.provider.Settings.Secure;
import android.app.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;

import android.app.AlertDialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.telephony.TelephonyManager;

import android.content.pm.PackageManager.NameNotFoundException;

import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

/**
 * Actividad principal para determinar si la aplicación esta registrada en el
 * servidor. Esta reenvia a la pantalla de ordenes si esta registrada o a la
 * pantalla de registro de lo contrario. Registra la aplicacion en Google GCM
 * 
 * @author Jose Suero
 * 
 */
public class SplashScreen extends Activity {

	/**
	 * Variable Global para identificar la pantalla en el log
	 */
	private static String TAG = "Splash Screen";

	private final BroadcastReceiver finishSplash = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	};

	private final BroadcastReceiver messageSplash = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			DisplayMessage(intent.getExtras().getString("message"));
		}
	};

	private final BroadcastReceiver checkVersion = new BroadcastReceiver() {

		@Override
		public void onReceive(Context ctx, Intent intent) {
			final String versionName;
			final String newVersion;
			final Context context = ctx;

			try {
				versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				newVersion = intent.getExtras().getString("responseText");
				
				double version = 0;
				
				try {
					version = Double.valueOf(newVersion);
				} catch (Exception e){
					version = Double.valueOf(versionName);
				
				}

				if (Double.valueOf(versionName) != version) {
					new AlertDialog.Builder(context)
							.setTitle("Nueva Version")
							.setMessage(
									"Existe una version mas reciente, actualizar?")
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
												UpdateAPK updateApp = new UpdateAPK();
									            updateApp.setContext(getApplicationContext());
									            updateApp.execute(Utils.concat("Facilidades",newVersion,".apk"));

										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
											verifyProcess();
										}
									}).show();
				} else {
					verifyProcess();
				}
			} catch (NameNotFoundException e) {
				DisplayMessage(e.toString());
			} catch (NumberFormatException e) {
				DisplayMessage(e.toString());
			}
		}
	};

	/**
	 * Evento de creacion de la pantalla
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		// Broadcast para recibir finish
		registerReceiver(finishSplash, new IntentFilter("finishSplash"));
		registerReceiver(messageSplash, new IntentFilter("messageSplash"));
		registerReceiver(checkVersion, new IntentFilter("checkVersion"));

		checkNewVersion();
	}

	private void verifyProcess() {
		boolean regComplete = (new AppValues(this).getBoolean("registration"));

		
    	//Start Service
    	Calendar cal = Calendar.getInstance();

    	Intent intent = new Intent(this, UpdateService.class);
    	PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

    	AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
    	// Start every 10 minutes
    	alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), Utils.MINUTOS_UPDATE * (60*1000), pintent); 

		
	    if(regComplete) {
	    	startActivity(new Intent(this, OrdenesList.class));
	    	finish();
	    } else {
	        checkNotNull(SERVER_URL, "SERVER_URL");
	        checkNotNull(SENDER_ID, "SENDER_ID");
	        //GCM implementation
	        /*
	        // Make sure the device has the proper dependencies.
	        GCMRegistrar.checkDevice(this);
	        // Make sure the manifest was properly set - comment out this line
	        // while developing the app, then uncomment it when it's ready.
	        GCMRegistrar.checkManifest(this);

		if (regComplete) {
			
			getContentResolver().delete(Orden.CONTENT_URI, "status = 1 and (julianday('now') - julianday(fecha_facilidades)) >=2", null);
			
			startActivity(new Intent(this, OrdenesList.class));
			finish();
		} else {
			checkNotNull(SERVER_URL, "SERVER_URL");
			checkNotNull(SENDER_ID, "SENDER_ID");
			// Make sure the device has the proper dependencies.
			GCMRegistrar.checkDevice(this);
			// Make sure the manifest was properly set - comment out this line
			// while developing the app, then uncomment it when it's ready.
			GCMRegistrar.checkManifest(this);
	        final String regId = GCMRegistrar.getRegistrationId(this);
	        */
	        final String regId = getDeviceId(this);
	        if (regId.equals("")) {
        		TextView regstr = (TextView)findViewById(R.id.registerStr);
        		regstr.setText("Error identificando su dispositivo, contacte a servicio");
	        	/*
	        	if (Utils.isOnline(this)){
	        		GCMRegistrar.register(this, SENDER_ID);	
	        	} else {
	        		//Toast.makeText(this, "No tiene conexion a internet", Toast.LENGTH_LONG).show();
	        		TextView regstr = (TextView)findViewById(R.id.registerStr);
	        		regstr.setText("No tiene conexion a internet");
	        	}
	        	*/
	        } else {
	        	(new AppValues(this)).put("regId", regId);
	            intent = new Intent(getBaseContext(), Registration.class);
	            intent.putExtra("regId", regId);
	            startActivity(intent);
	        	Log.v(TAG, "Already registered");
	        	finish();
	        }
	   }
	}

	/**
	 * Evento Creacion del menu de la actividad.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}

	/**
	 * Verifica que un objeto no es nulo
	 * 
	 * @param reference
	 *            Objeto a verificar
	 * @param name
	 *            Nombre del objeto
	 */
	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Error de config");
		}
	}

	private void checkNewVersion() {
		// Nueva version
		new SendDataTask(this).execute(
				Utils.concat(SERVER_URL, "/ws/checkVersion"), "", "");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(finishSplash);
		unregisterReceiver(messageSplash);
		unregisterReceiver(checkVersion);
	}
	
	private void DisplayMessage(String mess){
		TextView regstr = (TextView) findViewById(R.id.registerStr);
		regstr.setText(mess);
	}
    
    public static String getDeviceId(Context ctx)
    {
        TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

        String tmDevice = tm.getDeviceId();
        String androidId = Secure.getString(ctx.getContentResolver(), Secure.ANDROID_ID);
        //String serial = null;
        //if(Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) serial = Build.SERIAL;

        if(tmDevice != null) return "01" + tmDevice;
        if(androidId != null) return "02" + androidId;
        //if(serial != null) return "03" + serial;
        // other alternatives (i.e. Wi-Fi MAC, Bluetooth MAC, etc.)

        return null;
    }
    
}
