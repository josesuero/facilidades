package com.mstn.facilidades.comm;

import com.mstn.facilidades.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * Tarea para postear informacion los servicios
 * @author Jose Suero
 *
 */
public class SendDataTask extends AsyncTask<String, Integer, String[]> {
    // Do the long-running work in here
	private Context activity;
	
	
	public SendDataTask(Context activity){
		this.activity = activity;
	}
	
	
	/**
     * Funcion que ejecutara en el background recibe strings con:
     * [0] URL del servicio
     * [1] tarjeta del usuario
     * [2] regId de la equipo
     * [3] Data adicional del requerimiento en JSON
    */	
    protected String[] doInBackground(String... data) {
    	return Utils.sendData(activity, data);
    	/*
    	Log.i(TAG, "Send Data Started");
    	String url = "";
    	StringBuilder responseText = null;
    	try{
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("tarjeta", data[1] ));
	        nameValuePairs.add(new BasicNameValuePair("regId", data[2]));
	        if (data.length > 3){
	        	nameValuePairs.add(new BasicNameValuePair("data", data[3]));
	        }
	        url = data[0];
	    	HttpResponse response = Utils.postData(url, nameValuePairs);
	    	responseText = new StringBuilder();
	    	// Get the response
	    	BufferedReader rd = new BufferedReader
	    	  (new InputStreamReader(response.getEntity().getContent()));
	    	
	    	String line = "";
	    	while ((line = rd.readLine()) != null) {
	    	  responseText.append(line);
	    	}
	    	responseText.toString();
    	} catch (Exception e){
		  responseText = new StringBuilder(e.toString()); 
    	}
    	String[] response = new String[3];
    	response[0] = url.substring(url.lastIndexOf("/")+1);
    	response[1] = responseText.toString();
    	response[2] = data[1];
    	Log.i(TAG, "Send Data Ended");
    	return response;
    	*/
    }

    // This is called each time you call publishProgress()
    /**
     * Muestra el progreso 
     */
    protected void onProgressUpdate(Integer... progress) {
        //setProgressPercent(progress[0]);
    }

    // This is called when doInBackground() is finished
    /**
     *Evento al terminar el proceso
     * @param result resultado del proceso
     */
    protected void onPostExecute(String[] data) {
    	Intent i = new Intent(data[0]);
    	i.putExtra("responseText", data[1]);
    	i.putExtra("tarjeta", data[2]);
    	activity.getApplicationContext().sendBroadcast(i);
    }
}