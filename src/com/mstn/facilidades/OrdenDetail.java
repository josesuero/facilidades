package com.mstn.facilidades;

import com.mstn.facilidades.db.Ordenes.Orden;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.*;

/**
 * Actividad que muestra el detalle de la orden con todos sus campos
 * los campos se toman de un JSON que se encuentra en la tabla de ordenes
 * en el campo data
 * @author Jose Suero
 *
 */
public class OrdenDetail extends Activity {

	/**
	 * Variable de cursor que almacena el registro de la orden
	 */
	private Cursor mCursor;
	/**
	 * Variable que almacena el registro de la orden en texto
	 */
	private String mOriginalContent;
	/**
	 * Variable que apunta a la tabla que esta dentro de la pantalla
	 */
	private TableLayout mData;
	/**
	 * Variable para identificar la actividad en el Log
	 */
	private static final String TAG="Facilidades";

	/**
	 * Variable para determinar opci�n de menu para ir a pantalla de facilidades
	 */
	public static final int MENU_ITEM_FACILIDADES = Menu.FIRST;

	/**
	 * Campos a extraer de la tabla ordenes
	 */
	private static final String[] PROJECTION = new String[] {
		Orden._ID, // 0
		Orden.CODE, // 1
		Orden.ORDERTYPE, //2
		Orden.CLIENT, //3
		Orden.PRIORITY, //4
		Orden.DATA, //5
		Orden.FACILIDADES //6
	};
	
	private String data;
	
	private String facilidades;
	
	private final BroadcastReceiver finishOrderDetail = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	};

	
	/**
	 * Variable para determinar el numero de orden de campo de la columna codigo
	 */
	private static final int COLUMN_CODE = 1;
	//private static final int COLUMN_ORDERTYPE = 2;
	//private static final int COLUMN_CLIENT = 3;
	//private static final int COLUMN_PRIORITY = 4;
	/**
	 * Variable para determinar el numero de orden de campo de la columna data
	 */
	private static final int COLUMN_DATA = 5;
	private static final int COLUMN_FACILIDADES = 6;
	
	/**
	 * Evento de creacion de la pantalla, llena el cursor y asigna la referencia
	 * de la tabla principal (orderTable) a la variable mData
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orden_detail);
        registerReceiver(finishOrderDetail, new IntentFilter("finishOrderDetail"));
        
        final Intent intent = getIntent();
    	mData = (TableLayout) findViewById(R.id.orderTable);
        mCursor = getContentResolver().query(intent.getData(), PROJECTION, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();

            mData.removeAllViewsInLayout();
            
            setTitle("Orden No." + mCursor.getString(COLUMN_CODE));
            
            data = mCursor.getString(COLUMN_DATA);
            facilidades = mCursor.getString(COLUMN_FACILIDADES);
            
            JSONObject js;
            
            try {
				js = new JSONObject(data);
				for (int i = 0;i < js.names().length();i++){
					String name = js.names().getString(i);

					
					TableRow tr=new TableRow(this);
					tr.setLayoutParams(new LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT));
					
                    if (i % 2 == 0){
                    	tr.setBackgroundColor(Color.WHITE);
                    } else {
                    	tr.setBackgroundColor(Color.LTGRAY);
                    }


					TextView b=new TextView(this);
                    String str=String.valueOf(name);
                    b.setText(str);
                    b.setTextColor(Color.BLACK);
                    b.setTextSize(15);
                    b.setTypeface(null, Typeface.BOLD);
                    tr.addView(b);


                    TextView b1=new TextView(this);
                    b1.setPadding(10, 0, 0, 0);
                    b1.setTextSize(15);
                    String str1=js.getString(name);
                    b1.setText(str1);
                    b1.setTextColor(Color.BLACK);
                    tr.addView(b1);

                    mData.addView(tr);


                    final View vline1 = new View(this);
                    vline1.setLayoutParams(new                
                    TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                    vline1.setBackgroundColor(Color.WHITE);
                    mData.addView(vline1);  // add line below each row   

					
				}
			} catch (JSONException e) {
				//text.append("Error: ");
				//text.append(e.getMessage());
				Log.e(TAG,e.toString());
			}
            
            //mData.setTextKeepState(Html.fromHtml(text.toString()),BufferType.SPANNABLE);
                        
            if (mOriginalContent == null) {
                mOriginalContent = data;
            }

        } else {
            setTitle(getText(R.string.error_title));
            Log.e(TAG, getText(R.string.error_title).toString());
            //mData.setText(getText(R.string.error_message));
        }

        
    }

    /**
     * Evento creacion del menu
     * Toma las opciones de menu/order_detail
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.orden_detail, menu);
        return true;
    }
    
    /**
     * Evento al seleccionar opcion del menu, 
     * La primera opcion va a facilicdades
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int facilidadesType = 0;
    	switch (item.getItemId()) {
        case R.id.menu_facilidadesCobre:
        	facilidadesType = 1;
        	break;
        case R.id.menu_facilidadesFibra:
        	facilidadesType = 2;
        	break;
        case R.id.menu_facilidadesCobreFibra:
        	facilidadesType = 3;
        	break;

        case R.id.menu_facilidadesCobreD:
        	facilidadesType = 4;
        	break;
        case R.id.menu_facilidadesFrame:
        	facilidadesType = 5;
        	break;
        }
        Intent intent = new Intent(Intent.ACTION_EDIT, getIntent().getData());
        intent.putExtra("type", facilidadesType);
        intent.putExtra("orden", data);
        intent.putExtra("facilidades", facilidades);
    	startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    
    /**
     * Evento al activar la pantalla
     * Crea dinamicamente una tabla con los campos en JSON de data y los 
     * asigna a la tabla referenciada en mData
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Evento al desactivar la actividad
     */
    @Override
    protected void onPause() {
        super.onPause();
        //mCursor.close();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	unregisterReceiver(finishOrderDetail);
    }
    
}
