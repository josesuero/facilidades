package com.mstn.facilidades;

import com.mstn.facilidades.preferences.AppValues;
import com.mstn.facilidades.utils.Utils;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.view.Menu;

/**
 * Actividad para visualizar la tarjeta y el regId de la aplicacion
 * @author Jose Suero
 *
 */
public class Preferences extends PreferenceActivity {

    /**
     * Evento de creacion
     * Carga los campos del arvicho xml/preference
     */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        String versionName;
		try {
			versionName = getPackageManager().getPackageInfo(getPackageName(), 0 ).versionName;
	        setTitle(Utils.concat("Preferencias V." + versionName));
	        addPreferencesFromResource(R.xml.preference);
	        
	        EditTextPreference regId = (EditTextPreference)findPreference("regId");
	        regId.setText((new AppValues(this)).getString("regId"));
	        
	        EditTextPreference tarjeta = (EditTextPreference)findPreference("tarjeta");
	        tarjeta.setText((new AppValues(this)).getString("tarjeta"));

		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

    }

    /**
     * Evento de creaci�n del menu
     * Carga opciones de menu/preferences
     */
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.preferences, menu);
        return true;
    }
}
