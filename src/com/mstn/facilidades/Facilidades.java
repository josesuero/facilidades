package com.mstn.facilidades;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.mstn.facilidades.comm.SendDataTask;
import com.mstn.facilidades.db.Ordenes.Orden;
import com.mstn.facilidades.preferences.AppValues;
import com.mstn.facilidades.utils.Utils;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static com.mstn.facilidades.utils.Utils.SERVER_URL;

/**
 * Actividad para digitar las facilidades.
 * 
 * @author Jose Suero
 */
public class Facilidades extends Activity {

	/**
	 * Evento de creacion de la actividad Toma los campos de
	 * Layout/facilidades_edit.
	 * 
	 * @param savedInstanceState
	 *            the saved instance state
	 */
	int facilidadesType = 0;
	/**
	 * Guarda el detalle en Texto JSON de la orden
	 */
	String detalleTrabajo = "";
	/**
	 * Tag de la actividad para fines de LOG
	 */
	private static String TAG = "FACILIDADES";
	/**
	 * Detalle de la orden en Objeto JSON
	 */
	private JSONObject detalleJS;

	private JSONObject facilidades;

	ProgressDialog pDialog;

	BroadcastReceiver SaveFacilidades = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle extras = intent.getExtras();
			String responseText = extras.getString("responseText");
			try {
				if (!responseText.equals("true")) {
					throw new Exception(responseText);
				}
				// String tarjeta = extras.getString("tarjeta");
				Log.i(TAG, responseText);
				facilidades.remove("DETALLETRABAJO").toString();
				ContentValues values = new ContentValues();
				values.put(Orden.STATUS, "1");
				values.put(Orden.FECHA_FACILIDADES,
						facilidades.getString("FECHA"));
				values.put(Orden.FACILIDADES, facilidades.toString());
				context.getContentResolver().update(
						Orden.CONTENT_URI,
						values,
						Orden.CODE + " = '" + detalleJS.getString("Solicitud")
								+ "'", null);

				Toast.makeText(context, "Guardado", Toast.LENGTH_LONG).show();
				sendBroadcast(new Intent("finishOrderDetail"));
				finish();

			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				Toast.makeText(context, "Error Guardando", Toast.LENGTH_LONG)
						.show();
			}
			pDialog.dismiss();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facilidades_edit);
		registerReceiver(SaveFacilidades, new IntentFilter("SaveFacilidades"));
		facilidadesType = getIntent().getExtras().getInt("type");
		detalleTrabajo = getIntent().getExtras().getString("orden");

		String facilidadesStr = getIntent().getExtras()
				.getString("facilidades");
		JSONObject facilidadesJS = null;
		try {
			detalleJS = new JSONObject(detalleTrabajo);
			if (facilidadesStr != null) {
				facilidadesJS = new JSONObject(facilidadesStr);

				((TextView) findViewById(R.id.numerotelefono))
						.setText(facilidadesJS.getString("TELEFONO"));
				((TextView) findViewById(R.id.cabina)).setText(facilidadesJS
						.getString("CABINA"));
				((TextView) findViewById(R.id.parfeeder)).setText(facilidadesJS
						.getString("PARFEEDER"));
				((TextView) findViewById(R.id.terminal)).setText(facilidadesJS
						.getString("TERMINAL"));
				((TextView) findViewById(R.id.parlocal)).setText(facilidadesJS
						.getString("PARLOCAL"));

				String tipopuerto = facilidadesJS.getString("TIPOPUERTO");
				int indextipopuerto = 0;
				if (tipopuerto.equals("Alcaltel")) {
					indextipopuerto = 0;
				} else if (tipopuerto.equals("Stinger")) {
					indextipopuerto = 1;
				} else if (tipopuerto.equals("Zyxel")) {
					indextipopuerto = 2;
				} else if (tipopuerto.equals("Cisco")) {
					indextipopuerto = 3;
				}

				((Spinner) findViewById(R.id.tipopuerto))
						.setSelection(indextipopuerto);
				((TextView) findViewById(R.id.localidad)).setText(facilidadesJS
						.getString("LOCALIDAD"));
				((TextView) findViewById(R.id.dslam)).setText(facilidadesJS
						.getString("DSLAM"));
				((TextView) findViewById(R.id.puerto)).setText(facilidadesJS
						.getString("PUERTO"));
				((TextView) findViewById(R.id.terminalfo))
						.setText(facilidadesJS.getString("TERMINALFO"));
				((CheckBox) findViewById(R.id.direccionvalida))
						.setChecked(facilidadesJS.getBoolean("DIRECCION"));
			}
			((TextView) findViewById(R.id.direccion)).setText(detalleJS
					.getString("Direccion"));
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
		}

		switch (facilidadesType) {
		case 1:
			// facilidadesCobre
			setTitle(R.string.menu_facilidadesCobre);
			findViewById(R.id.tipopuertoline).setVisibility(View.GONE);
			findViewById(R.id.localidadline).setVisibility(View.GONE);
			findViewById(R.id.dslamline).setVisibility(View.GONE);
			findViewById(R.id.puertoline).setVisibility(View.GONE);
			findViewById(R.id.terminalfoline).setVisibility(View.GONE);

			break;
		case 2:
			// facilidadesFibra:
			setTitle(R.string.menu_facilidadesFibra);
			findViewById(R.id.parfeederline).setVisibility(View.GONE);
			findViewById(R.id.terminalline).setVisibility(View.GONE);
			findViewById(R.id.parlocalline).setVisibility(View.GONE);
			findViewById(R.id.dslamline).setVisibility(View.GONE);
			break;
		case 3:
			// facilidadesCobreFibra:
			setTitle(R.string.menu_facilidadesCobreFibra);
			findViewById(R.id.dslamline).setVisibility(View.GONE);
			break;
		case 4:
			// facilidadesCobreD:
			setTitle(R.string.menu_facilidadesCobreD);
			findViewById(R.id.terminalfoline).setVisibility(View.GONE);
			break;
		case 5:
			// facilidadesFrame:
			setTitle(R.string.menu_facilidadesFrame);
			findViewById(R.id.numerotelefonoline).setVisibility(View.GONE);
			findViewById(R.id.cabinaline).setVisibility(View.GONE);
			findViewById(R.id.terminalline).setVisibility(View.GONE);
			findViewById(R.id.parlocalline).setVisibility(View.GONE);
			findViewById(R.id.tipopuertoline).setVisibility(View.GONE);
			findViewById(R.id.localidadline).setVisibility(View.GONE);
			findViewById(R.id.dslamline).setVisibility(View.GONE);
			findViewById(R.id.terminalfoline).setVisibility(View.GONE);
			findViewById(R.id.direccionline).setVisibility(View.GONE);
			findViewById(R.id.direccionvalida).setVisibility(View.GONE);
			break;
		}

		EditText mCabina = (EditText) findViewById(R.id.cabina);
		mCabina.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String cabina = ((TextView) findViewById(R.id.cabina))
							.getText().toString();
					if (findViewById(R.id.terminalline).getVisibility() != View.GONE) {
						String terminal = ((TextView) findViewById(R.id.terminal))
								.getText().toString();
						if (terminal.equals("")
								|| terminal.split("-").length < 2)
							((TextView) findViewById(R.id.terminal))
									.setText(Utils.concat(cabina, "-"));
						else {
							((TextView) findViewById(R.id.terminal))
									.setText(Utils.concat(cabina, "-",
											terminal.split("-")[1]));
						}
					}
					if (findViewById(R.id.parlocalline).getVisibility() != View.GONE) {
						String parlocal = ((TextView) findViewById(R.id.parlocal))
								.getText().toString();
						if (parlocal.equals("")
								|| parlocal.split("-").length < 2)
							((TextView) findViewById(R.id.parlocal))
									.setText(Utils.concat(cabina, "-"));
						else {
							((TextView) findViewById(R.id.parlocal))
									.setText(Utils.concat(cabina, "-",
											parlocal.split("-")[1]));
						}
					}
				}
			}
		});
	}

	/**
	 * Evento creacion Opciones del menu toma las opciones de
	 * menu/facilidades_edit.
	 * 
	 * @param menu
	 *            the menu
	 * @return true, if successful
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.facilidades_edit, menu);
		return true;
	}

	/**
	 * Evento al seleccionar opciones del menu Envia las facilidades al
	 * servidor.
	 * 
	 * @param item
	 *            the item
	 * @return true, if successful
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_send:

			facilidades = new JSONObject();

			try {
				pDialog = ProgressDialog.show(this, "Enviando..",
						"Por favor espere", true, false);

				facilidades.put("NUMEROTRABAJO", detalleJS.getString("Codigo"));
				// TODO: buscar tipo de trabajo
				facilidades.put("TIPOTRABAJO", "1");
				facilidades.put("TIPOFACILIDADES",
						String.valueOf(facilidadesType));

				String currdate = new SimpleDateFormat("yyyy-MM-dd", Locale.US)
						.format(new Date());

				facilidades.put("FECHA", currdate);
				facilidades.put("TELEFONO",
						((TextView) findViewById(R.id.numerotelefono))
								.getText());
				facilidades.put("CABINA",
						((TextView) findViewById(R.id.cabina)).getText());
				facilidades.put("PARFEEDER",
						((TextView) findViewById(R.id.parfeeder)).getText());
				facilidades.put("TERMINAL",
						((TextView) findViewById(R.id.terminal)).getText());
				facilidades.put("PARLOCAL",
						((TextView) findViewById(R.id.parlocal)).getText());
				facilidades.put("TIPOPUERTO",
						((Spinner) findViewById(R.id.tipopuerto))
								.getSelectedItem().toString());
				facilidades.put("LOCALIDAD",
						((TextView) findViewById(R.id.localidad)).getText());
				facilidades.put("DSLAM",
						((TextView) findViewById(R.id.dslam)).getText());
				facilidades.put("PUERTO",
						((TextView) findViewById(R.id.puerto)).getText());
				facilidades.put("TERMINALFO",
						((TextView) findViewById(R.id.terminalfo)).getText());
				facilidades.put("DIRECCION",
						((CheckBox) findViewById(R.id.direccionvalida))
								.isChecked());
				facilidades.put("DETALLETRABAJO", detalleTrabajo);

				if (!validateForm(facilidades)) {
					pDialog.dismiss();
					return false;
				}

				// Toast.makeText(this, "Enviando", Toast.LENGTH_LONG).show();

				AppValues values = new AppValues(this);

				new SendDataTask(this).execute(SERVER_URL
						+ "/ws/SaveFacilidades", values.getString("tarjeta"),
						values.getString("regId"), facilidades.toString());

				// String responseText = task.get()[1];
			} catch (Exception e) {
				pDialog.dismiss();
				Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
			}

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean validateForm(JSONObject facilidades) throws JSONException {
		if (findViewById(R.id.numerotelefonoline).getVisibility() != View.GONE
				&& !Utils.IsMatch(facilidades.getString("TELEFONO"),
						"(809|829|849)(\\d{7})")) {
			Toast.makeText(this, "El telefono no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.numerotelefono)).requestFocus();
			return false;
		} else if (findViewById(R.id.cabinaline).getVisibility() != View.GONE
				&& !Utils.IsMatch(facilidades.getString("CABINA"),
						"([Cc])[A-Za-z0-9]*([A-Za-z]$)")) {
			//"([Cc])[A-Za-z]{4}(\\d{1,2}$)"
			Toast.makeText(this, "La cabina no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.cabina)).requestFocus();
			return false;
		} else if (findViewById(R.id.parfeederline).getVisibility() != View.GONE
				&& !Utils.IsMatch(facilidades.getString("PARFEEDER"),
						"^[A-Za-z]{1,2}\\d{1,2}\\-\\d{1,4}$")) {
			Toast.makeText(this,
					"El Par Feeder no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.parfeeder)).requestFocus();
			return false;
		} else if (findViewById(R.id.terminalline).getVisibility() != View.GONE
				&& !Utils
						.IsMatch(facilidades.getString("TERMINAL"),
								"([Cc])[A-Za-z0-9]*([A-Za-z])[\\-]([A-Za-z0-9]{1,4})$")) {
			Toast.makeText(this, "El Terminal no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.terminal)).requestFocus();
			return false;
		} else if (findViewById(R.id.parlocalline).getVisibility() != View.GONE
				&& !Utils.IsMatch(facilidades.getString("PARLOCAL"),
						"([Cc])[A-Za-z0-9]*([A-Za-z])[\\-]([0-9]{1,4})$")) {
			Toast.makeText(this, "EL par local no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.parlocal)).requestFocus();
			return false;
		} else if (findViewById(R.id.localidadline).getVisibility() != View.GONE
				&& !Utils.IsMatch(facilidades.getString("LOCALIDAD"),
						"^(?=\\s*\\S).*$")) {
			Toast.makeText(this, "La localidad no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.localidad)).requestFocus();
			return false;
		} else if (findViewById(R.id.tipopuertoline).getVisibility() != View.GONE) {
			String dslam = facilidades.getString("DSLAM");
			String puerto = facilidades.getString("PUERTO");
			boolean dslamcheck = true;
			boolean puertocheck = true;

			switch (((Spinner) findViewById(R.id.tipopuerto))
					.getSelectedItemPosition()) {
			case 0: // Alcatel
				if (findViewById(R.id.dslamline).getVisibility() != View.GONE
						&& !Utils.IsMatch(dslam, "^[aA][a-zA-Z]*\\d{1,3}$")) {
					dslamcheck = false;
				} else if (findViewById(R.id.puertoline).getVisibility() != View.GONE
						&& !Utils.IsMatch(puerto,
								"^\\d\\-\\d\\-\\d{2}\\-\\d{2}$")) {
					puertocheck = false;
				}
				break;
			case 1: // Stringer
				if (findViewById(R.id.dslamline).getVisibility() != View.GONE
						&& !Utils.IsMatch(dslam, "^[sS][a-zA-Z]*\\d{1,3}$")) {
					dslamcheck = false;
				} else if (findViewById(R.id.puertoline).getVisibility() != View.GONE
						&& !Utils.IsMatch(puerto, "^\\d\\-\\d{2}$")) {
					puertocheck = false;
				}
				break;
			case 2: // Zyxel
				if (findViewById(R.id.dslamline).getVisibility() != View.GONE
						&& !Utils.IsMatch(dslam, "^[zZ][a-zA-Z]*\\d{1,3}$")) {
					dslamcheck = false;
				} else if (findViewById(R.id.puertoline).getVisibility() != View.GONE
						&& !Utils.IsMatch(puerto, "^\\d\\-\\d{2}$")) {
					puertocheck = false;
				}
				break;
			case 3: // Stringer
				if (findViewById(R.id.dslamline).getVisibility() != View.GONE
						&& !Utils.IsMatch(dslam, "^[a-zA-Z]*\\d{1,3}$")) {
					dslamcheck = false;
				} else if (findViewById(R.id.puertoline).getVisibility() != View.GONE
						&& !Utils.IsMatch(puerto, "^\\d\\-\\d{2}$")) {
					puertocheck = false;
				}
				break;
			}
			if (!dslamcheck) {
				Toast.makeText(this, "El dslam no esta en un formato correcto",
						Toast.LENGTH_LONG).show();
				((TextView) findViewById(R.id.dslam)).requestFocus();
				return false;
			} else if (!puertocheck) {
				Toast.makeText(this,
						"El puerto no esta en un formato correcto",
						Toast.LENGTH_LONG).show();
				((TextView) findViewById(R.id.puerto)).requestFocus();
				return false;
			}
		} else if (findViewById(R.id.terminalfoline).getVisibility() != View.GONE
				&& !Utils.IsMatch(facilidades.getString("TERMINALFO"),
						"^FC[A-Za-z]{1,3}\\d[A-Za-z]{2}\\d$")) {
			Toast.makeText(this,
					"El Terminal FO no esta en un formato correcto",
					Toast.LENGTH_LONG).show();
			((TextView) findViewById(R.id.terminalfo)).requestFocus();
			return false;
		} else {
			return true;
		}
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(SaveFacilidades);
	}
}
