package com.mstn.facilidades.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mstn.facilidades.db.Ordenes.Orden;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;


/**
 * Clase para general que provee metodos y variable comunes a varias clases
 */
public final class Utils {
	/**
	 * Function para concatenar strings
	 * @param strs arreglo de Strings a concatenar
	 * @return un string concatenado
	 */
	public static String concat(Object... strs){
		StringBuilder builder = new StringBuilder("");
		for (int i= 0; i < strs.length;i++){
			builder.append(strs[i]);
		}
		return builder.toString();
	}

    /**
     * URL del servidor de aplicaciones (ej http://my_host:8080/gcm-demo)
     */
	//Produccion
	public static final String SERVER_URL = "http://172.27.6.201:7040/FacilidadesServer";
	//Prueba
	//public static final String SERVER_URL = "http://172.27.17.161:8020/FacilidadesServer";
	
	
	
    /**
     * Google API project id registrado para user GCM.
     */
	public static final String SENDER_ID = "250520824321";

	/**
     * Tag usada en los mensajes del LOG
     */
	public static final String TAG = "GCMDemo";

    /**
     * Intent usado para mostrar un message en la pantalla.
     */
	public static final String DISPLAY_MESSAGE_ACTION =
            "com.google.android.gcm.demo.app.DISPLAY_MESSAGE";

    /**
     * Intent's extra que contiene el mensaje a ser mostrado.
     */
	public static final String EXTRA_MESSAGE = "message";
	
	public static final long MINUTOS_UPDATE = 1;

    /**
     * Notifica el UI de un mensaje que debe ser mostrado
     * <p>
     *
     * @param context context de la application.
     * @param message message a mostrar.
     */
	public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
	
	/**
	 * Funcion para enviar data al servidor
	 * @param url Direccion del servidor
	 * @param values Lista de valores a enviar
	 * @return HTTPResponse con respuesta del servidor 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static HttpResponse postData(String url, List<NameValuePair> values) throws ClientProtocolException, IOException {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(url);

        httppost.setEntity(new UrlEncodedFormEntity(values));

        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);
        return response;
	} 
	
	/**
	 * Funcion para agregar una orden a la tabla
	 * @param orders Arreglo con campos de la orden
	 * @param context Contexto de la aplicación
	 * @param intent Intent de la aplicacion
	 * @return retorna un JSON con objeto de la orden
	 * @throws JSONException
	 */
	public static JSONObject addOrders(JSONArray orders, Context context, Intent intent) throws JSONException{
	    //int mState;
	    Uri mUri;
	    
	    JSONObject order = new JSONObject();
	    
	    for(int i =0;i < orders.length();i++){
	    	JSONObject item = orders.getJSONObject(i);
	    	order.put(item.getString("Key"), item.getString("Value"));
	    }
	    
	    ContentValues values = new ContentValues();
	    values.put(Orden.CLIENT, order.getString("Cliente"));
	    values.put(Orden.CODE, order.getString("Solicitud"));
	    values.put(Orden.DATA, order.toString());
	    values.put(Orden.ORDERTYPE, order.getString("FlagDato"));
	    values.put(Orden.FECHA_COMPROMISO, order.getString("Fecha Compromiso"));
	    values.put(Orden.STATUS, 0);
	    if (intent.getData() == null) {
            intent.setData(Orden.CONTENT_URI);
        }
	    
	    if (order.getString("OPERATION").equals("1")){
	    	mUri = context.getContentResolver().insert(intent.getData(), values);
	    } else {
	    	String[] selectionArgs = new String[1];
	    	selectionArgs[0] = order.getString("Solicitud");
	    	context.getContentResolver().delete(intent.getData(), "CODE=?", selectionArgs);
	    	mUri = intent.getData();
	    }
	    
        
        if (mUri == null){
        	order = null;
        }
		return order;
	}
	
	public static boolean isOnline(Context context) {
	    ConnectivityManager cm =
	        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}

	public static boolean IsMatch(String s, String pattern) {
        try {
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
          return false;
        }  

	}
	
	public static String[] sendData(Context activity, String... data){
		String TAG = "SendDataTask";
    	Log.i(TAG, "Send Data Started");
    	String url = "";
    	StringBuilder responseText = null;
    	try{
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("tarjeta", data[1] ));
	        nameValuePairs.add(new BasicNameValuePair("regId", data[2]));
	        if (data.length > 3){
	        	nameValuePairs.add(new BasicNameValuePair("data", data[3]));
	        }
	        url = data[0];
	    	HttpResponse response = Utils.postData(url, nameValuePairs);
	    	responseText = new StringBuilder();
	    	// Get the response
	    	BufferedReader rd = new BufferedReader
	    	  (new InputStreamReader(response.getEntity().getContent()));
	    	
	    	String line = "";
	    	while ((line = rd.readLine()) != null) {
	    	  responseText.append(line);
	    	}
	    	responseText.toString();
    	} catch (Exception e){
		  responseText = new StringBuilder(e.toString()); 
    	}
    	String[] response = new String[3];
    	response[0] = url.substring(url.lastIndexOf("/")+1);
    	response[1] = responseText.toString();
    	response[2] = data[1];
    	Log.i(TAG, "Send Data Ended");
    	return response;
		
	}

}
