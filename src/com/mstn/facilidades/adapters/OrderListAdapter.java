package com.mstn.facilidades.adapters;

import com.mstn.facilidades.R;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class OrderListAdapter extends SimpleCursorAdapter{
	public OrderListAdapter(Context context,int layout, Cursor c, String[] from, int[] to){
		super(context, layout, c, from, to);
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//return super.getView(position, convertView, parent);
		View view = super.getView(position, convertView, parent);
		
		Cursor c = getCursor();
	    c.moveToPosition(position);
	    int col = c.getColumnIndex("status");
	    boolean isNew = c.getInt(col) == 0;
	    if (isNew) {	
	    	((TextView)view.findViewById(R.id.OrderCode)).setTextColor(Color.BLACK);
	    	((TextView)view.findViewById(R.id.ClientName)).setTextColor(Color.BLACK);
	    	((TextView)view.findViewById(R.id.FechaCompromiso)).setTextColor(Color.BLACK);
	    	//view.setBackgroundColor(Color.LTGRAY);
	    } else {
	    	((TextView)view.findViewById(R.id.OrderCode)).setTextColor(Color.GREEN);
	    	((TextView)view.findViewById(R.id.ClientName)).setTextColor(Color.GREEN);
	    	((TextView)view.findViewById(R.id.FechaCompromiso)).setTextColor(Color.GREEN);
	    }
		return view;  
	}

}
