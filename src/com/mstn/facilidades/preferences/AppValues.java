package com.mstn.facilidades.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Clase para persitir la data en las variables de aplicacion de android
 * @author Jose Suero
 *
 */
public class AppValues {
	
	/**
	 * Variable para manejo de preferencias
	 */
	SharedPreferences prefs;
	
	/**
	 * Constructor de la clase
	 * @param context Context de aplicación
	 */
	public AppValues(Context context){
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	/**
	 * Obtiene el valor de una variable en String
	 * @param name Nombre de la variable
	 * @return Valor de la variable
	 */
	public String getString(String name){
		return prefs.getString(name, "");
	}
	/**
	 * Obtiene el valor de una variable en boolean
	 * @param name Nombre de la variable
	 * @return Valor de la variable
	 */
	public boolean getBoolean(String name){
	    return prefs.getBoolean("registration", false);
	}
	
	/**
	 * Guarda el valor de una variable en String
	 * @param name Nombre de la variable
	 * @param value Valor de la variable
	 */
	public void put(String name, String value){
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(name, value);
		editor.commit();
	}
	/**
	 * Guarda el valor de una variable en boolean
	 * @param name Nombre de la variable
	 * @param value Valor de la variable
	 */
	public void put(String name, boolean value){
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(name, value);
		editor.commit();
	}
}
