package com.mstn.facilidades.update;

import com.mstn.facilidades.db.Ordenes.Orden;
import com.mstn.facilidades.preferences.AppValues;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class UninstallAPK  extends AsyncTask<String,Void,Void>{
		private Context context;
		public void setContext(Context contextf){
		    context = contextf;
		}

		@Override
		protected Void doInBackground(String... filename) {
		      try {
/*
		    	  Uri packageURI = Uri.parse("package:com.mstn.facilidades");
		    	  Intent intent = new Intent(Intent.ACTION_DELETE, packageURI);
		    	  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    	  context.startActivity(intent);
*/
		    	  context.getContentResolver().delete(Orden.CONTENT_URI, "1=1", null);
		    	  AppValues values = new AppValues(context);
		    	  values.put("registration", false);
		    	  values.put("tarjeta", "");
		    	  
		    	  System.exit(0);
		    	  
		        } catch (Exception e) {
		            Log.e("UninstallApk", "error! " + e.getMessage());
		        }
		    return null;
		}
}
