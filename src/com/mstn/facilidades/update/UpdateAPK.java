package com.mstn.facilidades.update;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.mstn.facilidades.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class UpdateAPK extends AsyncTask<String,Void,Void>{
private Context context;
public void setContext(Context contextf){
    context = contextf;
}

@Override
protected Void doInBackground(String... filename) {
      try {
  		String apkPath = Utils.concat(Utils.SERVER_URL.replace("FacilidadesServer", "FacilidadesFiles"),"/",filename[0]);  		
  		String apk = "Facilidades.apk";

            URL url = new URL(apkPath);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            String PATH = Environment.getExternalStorageDirectory().getPath();
            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(file, apk);
            if(outputFile.exists()){
                outputFile.delete();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }
            fos.close();
            is.close();

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(PATH + "/"+ apk)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
            context.startActivity(intent);


        } catch (Exception e) {
            Log.e("UpdateAPP", "Update error! " + e.getMessage());
        }
    return null;
}}  


/*

public class UpdateAPK {
	
	

	public static void update(Context context, String filename) throws IOException {
		String apkPath = Utils.concat(Utils.SERVER_URL,"/apk/",filename);
		Uri apkUri = Uri.parse(apkPath);
				
		
		String apk = "Facilidades.apk";
		String fileAbsPath = Utils.concat("file://", context.getFilesDir()
				.getAbsolutePath(), "/files/", apk);

		
		new DefaultHttpClient().execute(new HttpGet(apkPath))
        .getEntity().writeTo(context.openFileOutput(apk, Context.MODE_PRIVATE));



//		Intent intent = new Intent();
//		intent.setAction(android.content.Intent.ACTION_VIEW);
//		intent.setDataAndType(Uri.parse(fileAbsPath),				"application/vnd.android.package-archive");
//		context.startActivity(intent);

		
		Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(apk)), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
        context.startActivity(intent);		

	}
}
*/
