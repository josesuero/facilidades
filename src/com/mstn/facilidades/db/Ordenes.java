package com.mstn.facilidades.db;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Clase para el modelo de datos de la tabla de ordenes
 * @author Jose Suero
 *
 */
public final class Ordenes {
	private Ordenes(){};
	
    /**
     * Clase de ordenes
     */
	public static final String AUTHORITY = "com.mstn.facilidades.Ordenes";
	
	
	/**
	 * Clase de Orden 
	 * @author Jose Suero
	 *
	 */
	public final static class Orden implements BaseColumns{
		private Orden(){}
		
        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/orden");

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of notes.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.facilidades.orden";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single note.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.facilidades.orden";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = "_id";
        
    	/**
    	 * Nombre de la table
    	 */
        public static final String TABLENAME = "orden";
    	/**
    	 * Nombre campo ID
    	 */
        public static final String ID = "_id";
    	/**
    	 * Nombre campo CODE
    	 */
        public static final String CODE = "code";
    	/**
    	 * Nombre Campo ORDERTYPE
    	 */
        public static final String ORDERTYPE = "order_type";
    	/**
    	 * Nombre campo PRIORITY
    	 */
        public static final String PRIORITY = "priority";
    	/**
    	 * Nombre campo CLIENT
    	 */
        public static final String CLIENT = "client_name";
    	/**
    	 * Nombre campo STATUS
    	 */
        public static final String STATUS = "status";
    	/**
    	 * Nombre campo FECHA_COMPROMISO
    	 */
        public static final String FECHA_COMPROMISO = "fecha_compromiso";
    	/**
    	 * Nombre campo FACILIDADES
    	 */
        public static final String FACILIDADES = "facilidades";
    	/**
    	 * Nombre campo FECHA_FACILIDADES
    	 */
        public static final String FECHA_FACILIDADES = "fecha_facilidades";
        /**
    	 * Nombre campo DATA
    	 */
        public static final String DATA = "data";
    	

	}

}
