package com.mstn.facilidades;

import static com.mstn.facilidades.utils.Utils.SENDER_ID;
import static com.mstn.facilidades.utils.Utils.SERVER_URL;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.mstn.facilidades.comm.SendDataTask;
import com.mstn.facilidades.preferences.AppValues;
import com.mstn.facilidades.update.UninstallAPK;
import com.mstn.facilidades.utils.Utils;

/**
 * Clase de manejo de eventos de GCM
 * @author Jose Suero
 *
 */
public class GCMIntentService extends GCMBaseIntentService {
	
	/**
	 * Variable global para identificar la actividad en el LOG
	 */
	private static final String TAG = "GCMIntentService";
	
	/**
	 * Constructor de la clase
	 */
	public GCMIntentService(){
		super(SENDER_ID);
	}

	/**
	 * Evento en el caso que se produzca un error con GCM
	 */
	@Override
	protected void onError(Context context, String message) {
		Intent intent = new Intent("messageSplash");
		intent.putExtra("message", message);
		sendBroadcast(intent);
		Log.e(TAG, message);
	}

    /**
     * Evento al recibir un mensaje de GCM
     * recibe un valor llamado value, el mismo contiene un JSON con:
     * 
     * type Tipo del mensaje recibido
     * 	info - Mensaje de información para ser desplegado al usuario
     * 	newOrders - Existen ordenes nuevas para el usuario en el servidor.
     * message Texto a desplegar al usuario.
     * data - informacion adicional para procesar el mensaje
     *  
     */
	@Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String message = intent.getStringExtra("value");
        try {
			JSONObject mess = new JSONObject(message);
			
			if (mess.getString("type").equals("OrdersGet") || mess.getString("type").equals("OrdersGetAll")){
				AppValues appValues = new AppValues(context);
				String tarjeta = appValues.getString("tarjeta");
				String regId = appValues.getString("regId");
				
	        	AsyncTask<String, Integer, String[]> task = new SendDataTask(context).execute(Utils.concat(SERVER_URL,"/ws/",mess.getString("type")), tarjeta, regId); 
	        	//AsyncTask<String, Integer, String> task = new GetOrdersTask().execute(getString(R.string.url_registration) + "/OrdenesGet.html", tarjeta, regId);
	        	JSONArray orders = new JSONArray(task.get()[1]);
	        	List<String> savedOrders = new ArrayList<String>();
	        	savedOrders.add(SERVER_URL + "/ws/OrdersUpdate");
	        	
	        	savedOrders.add(tarjeta);
	        	savedOrders.add(regId);
	        	JSONArray processedOrders = new JSONArray();
	        	for(int i = 0;i < orders.length();i++){
	        		JSONObject order = Utils.addOrders(orders.getJSONArray(i), context, intent);
	        		if (order != null){
	        			processedOrders.put(order.getString("Codigo"));
	        		}
	        	}
	        	savedOrders.add(processedOrders.toString());
	        	String[] taskData = new String[savedOrders.size()]; 
	        	savedOrders.toArray(taskData);
	        	AsyncTask<String, Integer, String[]> taskUpdate = new SendDataTask(context).execute(taskData);
	        	taskUpdate.get();
			} else if (mess.getString("type").equals("deleteData")){
				UninstallAPK updateApp = new UninstallAPK();
	            updateApp.setContext(getApplicationContext());
	            updateApp.execute("");
			}
			message = mess.getString("message");
			
		} catch (Exception e) {
			message = e.toString();
		}
        // notifies user
        generateNotification(context, message);
    }

	/**
	 * evento al terminar el registro con GCM, recibe el ID de registro y lo 
	 * envia a la actividad de registro.
	 */
	@Override
	protected void onRegistered(Context context, String registrationId) {
		(new AppValues(this)).put("regId", registrationId);
		Log.i(TAG, "Device registered: regId = " + registrationId);
		sendBroadcast(new Intent("finishSplash"));
		Intent intent = new Intent().setClass(getApplication(), Registration.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("regId", registrationId);
		getApplication().startActivity(intent);
    }

	/**
	 * Evento al eliminar el registro en GCM
	 */
	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		

	}

	/**
	 * Funcion para generar una notificacion de android
	 * @param context Contexto de aplicacción.
	 * @param message Mensaje a desplegar
	 */
    private static void generateNotification(Context context, String message) {
        //int icon = R.drawable.ic_stat_gcm;
    	int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, OrdenesList.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }
}
